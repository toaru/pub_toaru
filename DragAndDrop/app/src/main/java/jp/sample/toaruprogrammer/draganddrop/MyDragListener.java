package jp.sample.toaruprogrammer.draganddrop;

import android.util.Log;
import android.view.DragEvent;
import android.view.View;
import android.widget.TextView;

public class MyDragListener implements View.OnDragListener {

    private static final String TAG = "MyDragListener";
    private static final boolean DEBUG = true;

    @Override
    public boolean onDrag(View v, DragEvent event) {

        int action = event.getAction();

        String no = "100";
        TextView tv = (TextView)v.findViewById(R.id.my_layout_text1);
        if(tv == null) {
            Log.e(TAG, "TextView == null");
        }else{
            no = tv.getText().toString();
        }
        switch(action) {
            case DragEvent.ACTION_DRAG_STARTED:
                Log.d(TAG, "ACTION_DRAG_STARTED  view no = " + no);
                break;
            case DragEvent.ACTION_DRAG_LOCATION:
                Log.d(TAG, "ACTION_DRAG_LOCATION  view no = " + no);
                break;
            case DragEvent.ACTION_DROP:
                Log.d(TAG, "ACTION_DROP  view no = " + no);
                break;
            case DragEvent.ACTION_DRAG_EXITED:
                Log.d(TAG, "ACTION_DRAG_EXITED  view no = " + no);
                break;
            case DragEvent.ACTION_DRAG_ENTERED:
                Log.d(TAG, "ACTION_DRAG_ENTERED  view no = " + no);
                break;
            case DragEvent.ACTION_DRAG_ENDED:
                Log.d(TAG, "ACTION_DRAG_ENDED  view no = " + no);
                break;
            default:
                Log.d(TAG, "ACTION_DRAG_ENDED  view no = ??????????");
                break;
        }

        Log.d(TAG, "event x = " + event.getX() + " y = " + event.getY());

        return true;
    }
}
