package jp.sample.toaruprogrammer.draganddrop;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.ClipData;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        ////////////////////////////////////////////////////
        //
        // リスト表示用のフラグメントを設定しているだけです。
        // 今回の内容とは関係ありません。
        //
        ////////////////////////////////////////////////////

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MyListFragment fragment = new MyListFragment();
        FragmentManager mgr = getFragmentManager();
        FragmentTransaction ft = mgr.beginTransaction();

        ft.replace(R.id.my_layout_fragment, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);

        ft.commit();
    }

    public static class MyListItem {

        ////////////////////////////////////////////////////
        //
        // リストに表示するデータを保持しておくためのデータクラスです。
        //
        ////////////////////////////////////////////////////
        public String mTitle;
        public String mDescription;

    }

    public static class MyListFragment extends ListFragment
            implements AdapterView.OnItemLongClickListener {

        ////////////////////////////////////////////////////
        //
        // リスト表示用のフラグメントです。
        // onItemLongClickでstartDrag()を実行しDrag&Dropを開始しています。
        //
        ////////////////////////////////////////////////////

        @Override
        public View onCreateView(LayoutInflater inflater,
                                 ViewGroup container, Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);

            View view = inflater.inflate(R.layout.my_layout_list_fragment,
                    container, false);
            return view;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            getListView().setOnItemLongClickListener(this);
            MyListAdapter adapter = new MyListAdapter((Context) getActivity()
                    .getApplicationContext());

            // リストに表示するデータをアダプタに設定しています。
            for (int i = 0; i < 5; i++) {
                adapter.putItem(new MyListItem());
            }
            super.setListAdapter(adapter);
        }

        @Override
        public boolean onItemLongClick(AdapterView<?> parent,
                                       View view, int position, long id) {

            ClipData data = ClipData.newPlainText("dummy1", "dummy2");
            View.DragShadowBuilder shadow = new MyDragShadowBuilder(view);

            view.startDrag(data, shadow, view, 0);
            return true;
        }
    }
}
