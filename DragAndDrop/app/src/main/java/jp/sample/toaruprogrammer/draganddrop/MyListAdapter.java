package jp.sample.toaruprogrammer.draganddrop;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class MyListAdapter extends BaseAdapter {

    ////////////////////////////////////////////////////
    //
    // getView()内でViewにsetOnDragListenerしています。
    // これでドラッグ＆ドロップできるViewとして扱われます。
    //
    ////////////////////////////////////////////////////
    private Context mContext;
    private ArrayList<MainActivity.MyListItem> mItems = new ArrayList<MainActivity.MyListItem>();

    public MyListAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null) {
            convertView = LayoutInflater.from(mContext)
                    .inflate(R.layout.my_layout_list_item, null);
            // DragListenerを設定します。
            convertView.setOnDragListener(new MyDragListener());
        }
        TextView tv = (TextView)convertView.findViewById(R.id.my_layout_text1);
        tv.setText(String.valueOf(position));

        tv = (TextView)convertView.findViewById(R.id.my_layout_text2);
        tv.setText("Description " + String.valueOf(position));

        return convertView;
    }

    public void putItem(MainActivity.MyListItem item) {
        mItems.add(item);
    }
}