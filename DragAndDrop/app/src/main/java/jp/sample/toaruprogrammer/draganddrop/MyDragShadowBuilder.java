package jp.sample.toaruprogrammer.draganddrop;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.util.Log;
import android.view.View;

public class MyDragShadowBuilder extends View.DragShadowBuilder {


    public MyDragShadowBuilder(View view) {
        super(view);
    }

    @Override
    public void onDrawShadow(Canvas canvas) {

        ////////////////////////////////////////////////////
        //
        // ドラッグシャドウでデザインの変更
        //
        ////////////////////////////////////////////////////
        Log.d("DragShadow", "onDrawShadow");
        View view = getView();

        view.setDrawingCacheEnabled(true);
        view.destroyDrawingCache();

        Bitmap bitmap = view.getDrawingCache();
        canvas.drawBitmap(bitmap, 0f, 0f, null);
    }

    @Override
    public void onProvideShadowMetrics(Point outShadowSize, Point outShadowTouchPoint) {

        ////////////////////////////////////////////////////
        //
        // ドラッグシャドウの大きさや位置をここで設定します。
        //
        ////////////////////////////////////////////////////
        Log.d("DragShadow", "onProvideShadowMetrics");
        // 中央に設定
        outShadowTouchPoint.set(getView().getWidth()/2, getView().getHeight()/2);
        super.onProvideShadowMetrics(outShadowSize, outShadowTouchPoint);
    }
}